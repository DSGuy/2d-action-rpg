# Code written by Oladeji Sanyaolu (13/1/2022)

extends Camera2D

onready var topLeft : Position2D = $Limits/TopLeft
onready var bottomRight : Position2D = $Limits/BottomRight

func _ready() -> void:
	limit_top = topLeft.position.y
	limit_left = topLeft.position.x
	limit_bottom = bottomRight.position.y
	limit_right = bottomRight.position.x
	pass
