# Code written by Oladeji Sanyaolu

extends AnimatedSprite

func _ready() -> void:
	frame = 0
	
	play("Animate")
	self.connect("animation_finished", self, "_on_animation_finished")
	pass

func _on_animation_finished() -> void:
	queue_free()
	pass

