# Code written by Oladeji Sanyaolu

extends KinematicBody2D

const PlayerHurtSound : PackedScene = preload("res://tscn/PlayerHurtSound.tscn")

export(int) var ACCELERATION : int = 500
export(int) var MAX_SPEED : int = 80
export(int) var ROLL_SPEED : int = 125
export(int) var FRICTION : int = 500

enum {
	MOVE,
	ROLL,
	ATTACK,
}
var state = MOVE
var velocity : Vector2 = Vector2.ZERO
var roll_vector : Vector2 = Vector2.DOWN
var stats : Node = PlayerStats

onready var animationPlayer : AnimationPlayer = $AnimationPlayer
onready var animationTree : AnimationTree = $AnimationTree
onready var animationState : AnimationNodeStateMachinePlayback = animationTree.get("parameters/playback")
onready var swordHitbox : Area2D = $HitboxPivot/SwordHitbox
onready var hurtBox : Area2D = $Hurtbox
onready var blinkAnimationPlayer : AnimationPlayer = $BlinkAnimationPlayer
 
func _ready() -> void:
	stats.connect("no_health", self, "_on_player_no_health")
	
	hurtBox.connect("area_entered", self, "_on_hurtbox_area_entered")
	hurtBox.connect("invincibility_started", self, "_on_hurtbox_invincibility_started")
	hurtBox.connect("invincibility_ended", self, "_on_hurtbox_invincibility_ended")
	
	animationTree.active = true
	swordHitbox.knockback_vector = roll_vector
	pass

func _physics_process(dt : float) -> void:
	match state:
		MOVE:
			move_state(dt)
		ROLL:
			roll_state()
		ATTACK:
			attack_state()
	pass


# State Machine

func move_state(dt : float) -> void:
	var input_vector : Vector2 = Vector2.ZERO
	input_vector.x = Input.get_action_strength("ui_right") - Input.get_action_strength("ui_left")
	input_vector.y = Input.get_action_strength("ui_down") - Input.get_action_strength("ui_up")
	
	input_vector = input_vector.normalized()
	
	# Update animation blend positions as the input_vector > 0
	if input_vector != Vector2.ZERO:
		roll_vector = input_vector
		swordHitbox.knockback_vector = input_vector
		
		animationTree.set("parameters/Idle/blend_position", input_vector)
		animationTree.set("parameters/Run/blend_position", input_vector)
		animationTree.set("parameters/Attack/blend_position", input_vector)
		animationTree.set("parameters/Roll/blend_position", input_vector)
		animationState.travel("Run")
		velocity = velocity.move_toward(input_vector * MAX_SPEED, ACCELERATION * dt)
		#velocity += input_vector * ACCELERATION * dt
		#velocity = velocity.clamped(MAX_SPEED)
	else:
		animationState.travel("Idle")
		velocity = velocity.move_toward(Vector2.ZERO, FRICTION * dt)
	
	move() # Applies the whole physics
	
	# Input check
	if Input.is_action_just_pressed("roll"):
		state = ROLL
	
	if Input.is_action_just_pressed("attack"):
		state = ATTACK
	pass

func roll_state() -> void:
	velocity = roll_vector * ROLL_SPEED
	animationState.travel("Roll")
	move()
	pass

func attack_state() -> void:
	velocity = Vector2.ZERO
	animationState.travel("Attack")
	pass

# Functions

func move() -> void:
	velocity = move_and_slide(velocity)
	pass


# Callbacks

func _on_hurtbox_area_entered(area : Area2D) -> void:
	stats.health -= area.damage
	hurtBox.start_invincibility(0.6)
	hurtBox.create_hit_effect()
	
	var playerHurtSounds : AudioStreamPlayer = PlayerHurtSound.instance()
	var main : Node = get_tree().current_scene
	
	main.add_child(playerHurtSounds)
	pass

func _on_player_no_health() -> void:
	queue_free()
	pass

func roll_animation_finished() -> void:
	velocity = velocity * 0.8
	state = MOVE
	pass
	
func attack_animation_finished() -> void:
	state = MOVE
	pass

func _on_hurtbox_invincibility_started() -> void:
	blinkAnimationPlayer.play("Start")
	pass

func _on_hurtbox_invincibility_ended() -> void:
	blinkAnimationPlayer.play("Stop")
	pass
