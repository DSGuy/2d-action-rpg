# Code written by Oladeji Sanyaolu (20/12/2021)

extends Area2D


const HitEffect : PackedScene = preload("res://tscn/HitEffect.tscn")

var invincible : bool = false setget set_invincible

onready var timer : Timer = $Timer
onready var collisionShape : CollisionShape2D = $CollisionShape2D

signal invincibility_started
signal invincibility_ended

func _ready() -> void:
	timer.connect("timeout", self, "_on_timer_timeout")
	
	self.connect("invincibility_started", self, "_on_hurtbox_invincibility_started")
	self.connect("invincibility_ended", self, "_on_hurtbox_invincibility_ended")
	pass

func set_invincible(value : bool) -> void:
	invincible = value
	
	if invincible == true:
		emit_signal("invincibility_started")
	else:
		emit_signal("invincibility_end")
	pass

func start_invincibility(duration : float) -> void:
	self.invincible = true
	timer.start(duration)
	pass

func create_hit_effect() -> void:
	var hitEffect : Node2D = HitEffect.instance()
	var main : Node = get_tree().current_scene
	
	hitEffect.global_position = global_position - Vector2(0, 8)
	main.add_child(hitEffect)
	pass

func _on_timer_timeout() -> void:
	self.invincible = false
	pass

func _on_hurtbox_invincibility_started() -> void:
	collisionShape.set_deferred("disabled", true)
	pass

func _on_hurtbox_invincibility_ended() -> void:
	collisionShape.disabled = false
	pass
