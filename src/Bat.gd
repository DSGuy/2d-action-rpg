# Code written by Oladeji Sanyaolu

extends KinematicBody2D

export(int) var ACCELERATION : int = 300
export(int) var MAX_SPEED : int = 50
export(int) var FRICTION : int = 200
export(int) var WANDER_TARGET_RANGE : int = 4
const EnemyDeathEffect : PackedScene = preload("res://tscn/EnemyDeathEffect.tscn")

var knockback : Vector2 = Vector2.ZERO
var velocity : Vector2 = Vector2.ZERO
var state

onready var hurtBox : Area2D = $Hurtbox
onready var stats : Node = $Stats
onready var animatedSprite : AnimatedSprite = $AnimatedSprite
onready var playerDetection : Area2D = $PlayerDetection
onready var softCollision : Area2D = $SoftCollision
onready var wanderController : Node2D = $WanderController
onready var animationPlayer : AnimationPlayer = $AnimationPlayer

enum {
	IDLE,
	WANDER,
	CHASE,
}

func _ready() -> void:
	randomize()
	state = pick_random_state([IDLE, WANDER])
	
	hurtBox.connect("area_entered", self, "_on_hurtbox_area_entered")
	hurtBox.connect("invincibility_started", self, "_on_hurtbox_invincibility_started")
	hurtBox.connect("invincibility_ended", self, "_on_hurtbox_invincibility_ended")
	
	stats.connect("no_health", self, "_on_stats_no_health")
	pass

func _physics_process(dt : float) -> void:
	knockback = knockback.move_toward(Vector2.ZERO, FRICTION * dt)
	knockback = move_and_slide(knockback)
	
	match state:
		IDLE:
			velocity = velocity.move_toward(Vector2.ZERO, 200 * dt)
			seek_player()
			
			if wanderController.get_time_left() == 0:
				update_wander()
			pass
		WANDER:
			seek_player()
			if wanderController.get_time_left() == 0:
				update_wander()
			pass
			
			accelerate_towards_point(wanderController.target_position, dt)
			
			if global_position.distance_to(wanderController.target_position) <= WANDER_TARGET_RANGE:
				update_wander()
		CHASE:
			var player : Node = playerDetection.player
			if player:
				accelerate_towards_point(player.global_position, dt)
			else:
				state = IDLE
		
	if softCollision.is_colliding():
		velocity += softCollision.get_push_vector() * dt * 400
	velocity = move_and_slide(velocity)
	pass

func accelerate_towards_point(point : Vector2, dt : float) -> void:
	var distance : Vector2 = global_position.direction_to(point)
	#var distance : Vector2 = (player.global_position - global_position).normalized()
	velocity = velocity.move_toward(distance * MAX_SPEED, ACCELERATION * dt)
	animatedSprite.flip_h = velocity.x < 0
	pass

func update_wander() -> void:
	state = pick_random_state([IDLE, WANDER])
	wanderController.set_wander_timer(rand_range(1, 3))
	pass
func seek_player() -> void:
	if playerDetection.can_see_player():
		state = CHASE
	pass

func pick_random_state(state_list : Array):
	state_list.shuffle()
	return state_list.pop_front()
	pass

func _on_hurtbox_area_entered(area : Area2D) -> void:
	stats.health -= area.damage
	knockback = area.knockback_vector * 100

	hurtBox.start_invincibility(0.4)
	hurtBox.create_hit_effect()
	pass

func _on_stats_no_health() -> void:
	var enemyDeathEffect : Node2D = EnemyDeathEffect.instance()
	get_parent().add_child(enemyDeathEffect)
	enemyDeathEffect.global_position = global_position
	
	queue_free()
	pass

func _on_hurtbox_invincibility_started() -> void:
	animationPlayer.play("Start")
	pass

func _on_hurtbox_invincibility_ended() -> void:
	animationPlayer.play("Stop")
	pass
