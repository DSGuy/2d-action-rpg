# Code written by Oladeji Sanyaolu

extends Area2D

var player : Node = null

func _ready() -> void:
	connect("body_entered", self, "_on_body_entered")
	connect("body_exited", self, "_on_body_exited")
	pass

func can_see_player() -> bool:
	return player != null
	pass

func _on_body_entered(body : Node) -> void:
	player = body
	pass

func _on_body_exited(_body : Node) -> void:
	player = null
	pass
