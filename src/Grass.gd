# Code written by Oladeji Sanyaolu

extends Node2D

onready var hurtBox : Area2D = $Hurtbox

const GrassEffect : PackedScene = preload("res://tscn/GrassEffect.tscn")

func _ready() -> void:
	hurtBox.connect("area_entered", self, "_on_hurtbox_area_entered")
	pass

func create_grass_effect() -> void:
	var grassEffect : Node2D = GrassEffect.instance()
	
	grassEffect.global_position = global_position
	get_parent().add_child(grassEffect)
	
	pass

func _on_hurtbox_area_entered(_area : Area2D) -> void:
	create_grass_effect()
	queue_free()
	pass
