# Code written by Oladeji Sanyaolu (UI) (10/1/2022)

extends Control

var hearts : int = 4 setget set_hearts
var max_hearts : int = 4 setget set_max_hearts

onready var heartUIFull : TextureRect = $HeartUIFull
onready var heartUIEmpty : TextureRect = $HeartUIEmpty

func set_hearts(value : int) -> void:
	# warning-ignore:narrowing_conversion
	hearts = clamp(value, 0, max_hearts)
	
	if heartUIFull != null:
		heartUIFull.rect_size.x = hearts * 15
	
func set_max_hearts(value : int) -> void:
	# warning-ignore:narrowing_conversion
	max_hearts = max(value, 1)
	
	# warning-ignore:narrowing_conversion
	self.hearts = min(hearts, max_hearts)
	
	if heartUIEmpty != null:
		heartUIEmpty.rect_size.x = max_hearts * 15

func _ready() -> void:
	self.max_hearts = PlayerStats.max_health
	self.hearts = PlayerStats.health
	
	#warning-ignore:return_value_discarded
	PlayerStats.connect("health_changed", self, "set_hearts")
	#warning-ignore:return_value_discarded
	PlayerStats.connect("max_health_changed", self, "set_max_hearts")
