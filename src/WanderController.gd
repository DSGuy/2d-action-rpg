# Code written by Oladeji Sanyaolu (11/1/2022)

extends Node2D

export(int) var wander_range : int = 32

onready var start_position : Vector2 = global_position
onready var target_position : Vector2 = global_position

onready var timer : Timer = $Timer

func _ready() -> void:
	update_target_position()
	timer.connect("timeout", self, "_on_timer_timeout")
	pass

func update_target_position() -> void:
	var target_vector : Vector2 = Vector2(rand_range(-wander_range, wander_range), rand_range(-wander_range, wander_range))
	target_position = start_position + target_vector
	pass

func set_wander_timer(duration : float) -> void:
	timer.start(duration)
	pass

func get_time_left() -> float:
	return timer.time_left
	pass

func _on_timer_timeout() -> void:
	update_target_position()
	pass
